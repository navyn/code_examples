<?php
	list($delivery, $cartItems, $deliveryFee) = $model;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" 
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
		<title></title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<style type="text/css">
			/*
			Copyright (c) 2010, Yahoo! Inc. All rights reserved.
			Code licensed under the BSD License:
			http://developer.yahoo.com/yui/license.html
			version: 3.2.0
			build: 2676
			*/
			html{color:#000;background:#FFF;}body,div,dl,dt,dd,ul,ol,li,h1,h2,h3,h4,h5,h6,pre,code,form,fieldset,legend,input,textarea,p,blockquote,th,td{margin:0;padding:0;}table{border-collapse:collapse;border-spacing:0;}fieldset,img{border:0;}address,caption,cite,code,dfn,em,strong,th,var{font-style:normal;font-weight:normal;}li{list-style:none;}caption,th{text-align:left;}h1,h2,h3,h4,h5,h6{font-size:100%;font-weight:normal;}q:before,q:after{content:'';}abbr,acronym{border:0;font-variant:normal;}sup{vertical-align:text-top;}sub{vertical-align:text-bottom;}input,textarea,select{font-family:inherit;font-size:inherit;font-weight:inherit;}input,textarea,select{*font-size:100%;}legend{color:#000;}
			
			
			html,
			body {
				/*background-color: #fcffee;*/
				background-color: #ffffff;
				margin: 0;
				padding: 0;
				font-family: arial;
			}

			strong {
				font-weight: bold;
			}

			h3 {
				font-size: 14pt;
				font-weight: bold;
				margin: 20px 2.5px;
			}

			a { 
				color: #0c3b38;
			}
			
			.cart-table {
				width: 600px;
				border: 1px solid grey;
				margin: 10px 0;
				font-size: 8pt;
			}
			
			.cart-table th {
				text-align: center;
				padding: 10px;
			}
			
			.cart-table td {
				padding: 10px;
				font-weight: bold;
			}
			
			.cart-column-header {
				border: 1px solid grey;
			}
			
			.cart-item {
				
			}
			
			.cart-item-number {
				border-left: 1px solid grey;
				border-right: 1px solid grey;
				text-align: center;
				width: 20px;
			}
			
			.cart-item-name {
			}
			
			.cart-item-quantity {
				border-left: 1px solid grey;
				border-right: 1px solid grey;
				text-align: center;
				width: 30px;
			}
			
			td.cart-item-price {
				border-left: 1px solid grey;
				border-right: 1px solid grey;
				width: 100px;
			}
			
			td.cart-total-label {
				text-align: right;
				border: 1px solid grey;
			}
			
			td.cart-total-price {
				border: 1px solid grey;
			}
			
			#invoice {
				width: 610px;
				padding: 20px 0px 10px 20px;
			}
			
			#invoice-table {
				width: 610px;
				font-size: 10pt;
			}
			
			#order-number {
				font-size: 13pt;
				font-weight: bold;
				text-decoration: underline;
			}
			
			#delivery-recipient {
				font-weight: bold;
			}
			
			#delivery-date {
				font-weight: bold;
			}
			
			#delivery-location {
				font-weight: bold;
				font-style: italic;
				text-decoration: underline;
			}
			
			p {
				line-height: 20px;
				font-size: 10pt;
			}
			
		</style>
	</head>
	
	<body>
		<div id="invoice">
		<table id="invoice-table">
			<tr><td style="padding: 0 0 15px 0; font-size: 11pt;">Thank you for you order.</td></tr>
			<tr>
				<td style="padding: 0 0 15px 0; font-size: 11pt;">Your order number is <span id="order-number"><?php print $delivery->order_number; ?></span></td>
			</tr>
			<tr>
				<td style="padding: 0 0 10px 0;">
				<p>
				Delivery will be made to <span id="delivery-recipient"><?php print $delivery->name; ?></span>
				at <span id="delivery-location"><?php print "{$delivery->address1}, " . ($delivery->address2 ? "{$delivery->address2}, " : "") . "{$delivery->zip} {$delivery->city}, {$delivery->state}"; ?></span> on
				<span id="delivery-date"><?php print date_format(date_create($delivery->delivery_date), "l, jS F"); ?></span>
				</p>
				<?php if(strlen($delivery->additional_instructions) > 0) { ?>
				<p>
				Additional instructions, <?php print $delivery->additional_instructions; ?>
				</p>
				<?php } ?>
				</td>
			</tr>
			<tr>
				<td>
		<table class="cart-table">
		<thead>
		<tr class="cart-column-header">
			<th></th>
			<th>Item</th>
			<th>Qty</th>
			<th>Price</th>
		</tr>
		</thead>
		<tbody>
<?php
		$totalPrice = 0;
		if($cartItems)
		foreach($cartItems as $i => $cartItem) {
			$number = $i + 1;
			$quantityPrice = $cartItem['price'] * $cartItem['quantity'];
			$totalPrice += $quantityPrice;
			$quantityPrice = number_format($quantityPrice, 2, '.', '');
			$itemName = ucwords(strtolower($cartItem['name']));
?>
			<tr class="cart-item">
				<td class="cart-item-number"><?php print $number ?></td>
				<td class="cart-item-name"><?php print "{$itemName} ({$cartItem['product_id']})"; ?></td>
				<td class="cart-item-quantity"><?php print $cartItem['quantity']; ?></td>
				<td class="cart-item-price"><?php print "{$cartItem['currency_prefix']} {$quantityPrice}"; ?></td>
			</tr>
<?php
		}
		$totalPrice += $deliveryFee;
		$deliveryFee = number_format($deliveryFee, 2, '.', '');
		$totalPrice = number_format($totalPrice, 2, '.', '');
?>
		<tr class="cart-item">
			<td colspan="3" class="cart-total-label">Delivery</td>
			<td class="cart-total-price"><?php print "{$cartItems[0]['currency_prefix']} {$deliveryFee}"; ?></td>
		</tr>
		<tr class="cart-item">
			<td colspan="3" class="cart-total-label">Total</td>
			<td class="cart-total-price"><?php print "{$cartItems[0]['currency_prefix']} {$totalPrice}"; ?></td>
		</tr>
		</tbody>
		</table>
		
				</td>
			</tr>
		</table>
	</div>
	</body>
</html>