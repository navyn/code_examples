<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" 
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
		<title>Contact Us - Kosova Flowers</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="description" content="Kosova Flowers is the premier flower, chocolate and gift delivery service in Kosova. We try to make your ordering process simple and worry free to anywhere we deliver around Kosovo, Pristina and Ferizaj. Our prices are cheaper than the others and arrangements beautiful." />
		<meta name="keywords" content="kosova, kosovo, pristina, ferizaj, flower, flowers, chocolate, gift, gifts, delivery" />
		<link href="<?php print PUBLIC_URL; ?>/css/reset-min.css" rel="stylesheet" type="text/css" />
		<link href="<?php print PUBLIC_URL; ?>/css/default.css" rel="stylesheet" type="text/css" />
		<script src="<?php print PUBLIC_URL; ?>/jquery/jquery-1.4.4.js"></script>
		<script type="text/javascript">
			$(function(){

			});
		</script>
		<style type="text/css">
			
			div {
				/*border: 1px solid red;*/
			}
			
			h3 {
				position: relative;
				left: 3em;
				font-size: 10pt;
				text-decoration: underline;
				color: #4d1a03;
			}
			
			h3 > a {
				font-size: 10pt;
				text-decoration: underline;
				margin: 20px 0;
				color: #4d1a03;
			}
			
		</style>
	</head>
	
	<body>
	<div id="wrapper">
		<?php dispatch("Banner"); ?>
		<?php include_once(dirname(__FILE__)."/leftbar.php"); ?>
		
		<div id="content-box">
		
		<h3><a name="delivery">Contact Us</a></h3>
		
		<p class="text1" style="text-align: justify;">
		Please do not hesitate to contact us with any questions. Kindly include
		your order number with the email if it is regarding an order.
		Alternatively, you can just hit reply on your order confirmation email.
		We prefer communication through email, as we have a record of the
		communication and we also usually reply even after business hours.
		</p>
		
		<p class="text1">
		<strong>Send your support related questions to <a href="emailto:support@kosovaflowers.com"><i>support@kosovaflowers.com</i></a>.</strong>
		</p>
		
		<p class="text1">
		Kosova Operating hours: Mon - Sat, 9am - 6pm<br/>
		USA Operating hours: Mon - Sat, 9am - 6pm ET<br/>
		<br/>
		Kosova Phone: <b>+386 49 595-696</b><br/>
		<br/>
		USA Phone: <b>+1 (774) 2KS-LULE</b><br/>
		USA Phone: <b>+1 (774) 257-5853</b><br/>
		<br/>
		Email: support@kosovaflowers.com<br/>
		<br/>
		<u>USA Office:</u><br/>
		1870 Niagara Falls Blvd. #310<br/>
		Tonawanda, NY 14150, USA<br/>
		<br/>
		
		</div>
		
		<?php include_once(dirname(__FILE__)."/footer.php"); ?>
	</div>
	</body>
	
</html>