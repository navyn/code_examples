<style type="text/css">
	
	#left-bar {
		position: relative;
		/*background-color: #a5b75e;*/
		float: left;
		width: 160px;
		z-index: 128;
	}
	
	#nav-bar {
		margin-top: 0px;
		width: 165px;
		height: 400px;
		float: left;
		padding: 20px 0;
		background: url('<?php print PUBLIC_URL . "/images/leftbar4.png"; ?>') no-repeat;
	}
	
	.left-item {
		font-family: arial;
		font-size: 11pt;
		padding: 2.5px 15px;
		/*background-color: #67b051;*/
		color: #380000;
		cursor: pointer;
	}
	
	.left-item a {
		color: #380000;
		text-decoration: none;
	}
	
	.left-item a:hover {
		text-decoration: underline;
	}
	
</style>

<div id="left-bar">
	<div id="nav-bar">
		<div class="left-item"><a href="<?php print REL_ROOT_URL; ?>/flowers">Flowers</a></div>
		<div class="left-item"><a href="<?php print REL_ROOT_URL; ?>/chocolates">Chocolates</a></div>
	</div>
</div>