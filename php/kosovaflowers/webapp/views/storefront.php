<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" 
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
		<title>Kosova Flowers - Send Flowers To Kosovo Pristina</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="description" content="Kosova Flowers is the premier flower, chocolate and gift delivery service in Kosova. We try to make your ordering process simple and worry free to anywhere we deliver around Kosovo, Pristina and Ferizaj. Our prices are cheaper than the others and arrangements beautiful." />
		<meta name="keywords" content="kosova, kosovo, pristina, ferizaj, flower, flowers, chocolate, gift, gifts, delivery" />
		<link href="<?php print PUBLIC_URL; ?>/css/reset-min.css" rel="stylesheet" type="text/css" />
		<link href="<?php print PUBLIC_URL; ?>/css/default.css" rel="stylesheet" type="text/css" />
		<script src="<?php print PUBLIC_URL; ?>/jquery/jquery-1.4.4.js"></script>
		<script type="text/javascript">
			$(function(){

			});
		</script>
		<style type="text/css">
			
			div {
				/*border: 1px solid red;*/
			}
			
			.item-placeholder {
				padding: 5px 0;
				vertical-align: top;
			}
			
			.item-box {
				border: 2px solid #380000;
				border-top: 1px solid #380000;
				border-left: 1px solid #380000;
				margin: 3px;
				padding: 3px;
				width: 160px;
				height: 210px;
				text-align: center;
				background-color: #e4e7d9;
			}
			
			.item-image-default {
				width: 160px;
				height: 155px;
				background-color: white;
				/*height: 130px;*/
				display:table-cell;
				vertical-align: middle;
			}
			
			.item-image-default img {
				max-width: 160px;
				max-height: 155px;
			}
			
			.item-info {
				margin: 5px 0;
				padding: 0px;
			}
			
			.item-title {
				font-size: 10pt;
				font-weight: bold;
				padding: 3px 1px;
				height: 25px;
			}
			
			.item-short-desc {
				font-size: 9pt;
				padding: 0px 2.5px 2.5px 2.5px;
				height: 28px;
				overflow: hidden;
				color: #2a2a2a;
			}
			
			.item-price {
				font-size: 10pt;
				font-weight: bold;
				font-style: italic;
				padding: 5px;
				width: 90px;
			}
			
			.item-add-to-cart {
				float: left;
			}
			
			.item-add-to-cart input {
				font-size: 10pt;
				width: 70px;
			}
			
			#catalog-box {
				width: auto;
				margin-left: 15px;
				margin-top: 15px;
			}
			
		</style>
	</head>
	
	<body>
	<div id="wrapper">
		<?php dispatch("Banner"); ?>
		<?php include_once(dirname(__FILE__)."/leftbar.php"); ?>
		
		<div id="content-box">
		
		<p class="text1">
		Welcome to Kosova Flowers. We deliver flowers, chocolates and other
		gifts, across the Republic of Kosova at an affordable price. Our
		flowers and gifts are cheaper than most of the other sites and we
		intend to keep improving on the prices as our network expands.
		</p>
		
		<p class="text1">
		We take special care to ensure that your order gets delivered on time,
		with the arrangement and best flowers possible. Keep tuned as we add
		more flowers and gifts.
		</p>
		
		<div id="catalog-box">
		<table>
		<tbody>
		
		<?php
			global $paths;

			$o = "";
			$items = $model;
			$itemCount = count($items);
			$i = 0;
			while($i < $itemCount) {
				$actionUrl = formAction("cart");
				$o .= "<tr>";
				for($j = 0; $j < 4; $j++) {
					if($i < $itemCount) {
						$item = $items[$i++];
						$itemPrice = number_format($item['price'], 2, '.', '');
						$o .= <<<EOS
							<td class="item-placeholder">
								<form action="{$actionUrl}" method="post">
								<div class="item-box">
									<a href="{$paths['REL_ROOT_URL']}/items/{$item['seo_name']}">
										<div class="item-image-default">
											<img src="{$item['image_default_path']}" alt="{$item['name']}" title="{$item['short_description']}"/>
										</div>
									</a>
									<div class="item-info">
										<!--<div class="item-title">{$item['name']}</div>-->
										<div class="item-short-desc">{$item['short_description']}</div>
										<div class="item-price">{$item['currency_prefix']} {$itemPrice}</div>
										<!--<div class="item-add-to-cart"><input name="add" type="submit" value="add to cart"/></div>-->
									</div>
									<input name="do" type="hidden" value="add"/>
									<input name="itemId" type="hidden" value="{$item['id']}"/>
								</div>
								</form>
							</td>
EOS;
					}
				}
				$o .= "</tr>";
			}
			
			print $o;
		?>
		
		</tbody>
		</table>
		</div>
		
		</div>
		
		<?php include_once(dirname(__FILE__)."/footer.php"); ?>
	</div>
	</body>
	
</html>