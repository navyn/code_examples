<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" 
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
		<title>About Us</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link href="<?php print PUBLIC_URL; ?>/css/reset-min.css" rel="stylesheet" type="text/css" />
		<link href="<?php print PUBLIC_URL; ?>/css/default.css" rel="stylesheet" type="text/css" />
		<script src="<?php print PUBLIC_URL; ?>/jquery/jquery-1.4.4.js"></script>
		<script type="text/javascript">
			$(function(){

			});
		</script>
		<style type="text/css">
			
			div {
				/*border: 1px solid red;*/
			}
			
			h3 {
				position: relative;
				left: 3em;
				font-size: 10pt;
				text-decoration: underline;
				color: #4d1a03;
			}
			
			h3 > a {
				font-size: 10pt;
				text-decoration: underline;
				margin: 20px 0;
				color: #4d1a03;
			}
			
		</style>
	</head>
	
	<body>
	<div id="wrapper">
		<?php dispatch("Banner"); ?>
		<?php include_once(dirname(__FILE__)."/leftbar.php"); ?>
		
		<div id="content-box">
		
		<h3><a name="about">About Us</a></h3>
		
		<p class="text1">
		KosovaFlowers was established in 2010. Although a relatively young
		company, we are currently the premier online flower and gift delivery
		company in Kosova. Our online delivery prices are lower and more
		affordable than our competitors because we are striving to run a
		sustainable and humane business.
		</p>
		
		<p class="text1">
		Our objective is to provide flower and gift delivery service throught
		Kosova at very reasonable prices. We also want to make flower and gift
		delivery in Kosova easier and more reliable for everyone. 
		Especially for people living outside of Kosova and for those who don't 
		speak Shqip.
		</p>
		
		<h3><a name="partnership">Partnership</a></h3>
		
		<p class="text1">
		We work very closely with our delivery network to ensure you get the
		exact flowers, arrangement and gifts you purchase. We make sure every
		order that goes out meet our strict quality control standards.
		</p>
		
		<p class="text1">
		We are always looking to expand our florist and gift network in order
		to provide more products at competitive prices throughout Kosova. 
		</p>
		
		<p class="text1">
		<strong>
		If you would like to be part of our network and expand your business.
		Please contact us at 
		<a style="font-style: italic;" href="emailto:partners@kosovaflowers.com">partners@kosovaflowers.com</a>.
		</strong>
		</p>
		
		</div>
		
		<?php include_once(dirname(__FILE__)."/footer.php"); ?>
	</div>
	</body>
	
</html>