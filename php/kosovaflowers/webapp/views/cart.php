<?php
	list($cartItems, $currencyPrefix, $msg) = $model;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" 
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
		<title>Shopping cart - Kosova Flowers</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="description" content="Kosova Flowers is the premier flower, chocolate and gift delivery service in Kosova. We try to make your ordering process simple and worry free to anywhere we deliver around Kosovo, Pristina and Ferizaj. Our prices are cheaper than the others and arrangements beautiful." />
		<meta name="keywords" content="kosova, kosovo, pristina, ferizaj, flower, flowers, chocolate, gift, gifts, delivery" />
		<link href="<?php print PUBLIC_URL; ?>/css/reset-min.css" rel="stylesheet" type="text/css" />
		<link href="<?php print PUBLIC_URL; ?>/css/default.css" rel="stylesheet" type="text/css" />
		<style type="text/css">
			
			#content-box {
				position: relative;
				left: 0px;
				width: 100%;
				padding: 0;
				margin: 0;
			}
			
			#cart-box {
				width: auto;
				min-width: 600px;
				max-width: 800px;
				margin-left: 180px;
				margin-top: 15px;
				font-family: Arial, Helvetica, sans-serif;
				font-size: 10pt;
			}
			
			.cart-table {
				width: 95%;
				margin: 0 10px;
			}
			
			.cart-table caption {
				font-size: 10pt;
			}
			
			.cart-table th {
				text-align: center;
				padding: 10px;
				font-size: 9pt;
				background-color: #f0efab;
				letter-spacing: 1px;
			}
			
			.cart-table td {
				padding: 5px 5px;
			}
			
			.cart-column-header {
				border: 5px solid #fdfddd;
			}
			
			.cart-item {
				
			}
			
			.cart-item-number {
				border-top: 1px solid #9e9c30;
				border-bottom: 1px solid #9e9c30;
				text-align: center;
			}
			
			.cart-item-name {
				border-top: 1px solid #9e9c30;
				border-bottom: 1px solid #9e9c30;
			}
			
			.cart-item-name table {
				width: 300px;
			}
			
			.cart-item-name-picture {
				width: 70px;
				text-align: center;
				
			}
			
			.cart-item-name-picture img {
				height: 65px;
			}
			
			.cart-item-name-info {
				vertical-align: top;
			}
			
			.cart-item-name-name {
				font-weight: bold;
				padding-left: 10px;
			}
			
			.cart-item-name-productid {
				font-size: 8pt;
				font-style: italic;
				padding-left: 10px;
			}
			
			.cart-item-quantity {
				border-top: 1px solid #cd9c6d;
				border-bottom: 1px solid #cd9c6d;
				text-align: center;
			}
			
			td.cart-item-price {
				border-top: 1px solid #b38960;
				border-bottom: 1px solid #b38960;
				width: 85px;
				text-align: center;
				color: #2e514f;
				font-size: 9pt;
				font-weight: bold;
			}
			
			.cart-empty {
				text-align: right;
				font-style: italic;
				font-size: 10pt;
				text-align: center;
			}
			
			.cart-total {
				font-family: Arial, Helvetica, sans-serif;
				font-weight: bold;
				color: #380000;
			}
			
			.cart-total td {
				padding: 25px 0;
			}
			
			td.cart-total-label {
				padding-right: 30px;
				text-align: right;
				font-size: 11pt;
				letter-spacing: 1px;
			}
			
			td.cart-total-price {
				width: 70px;
				text-align: center;
				font-size: 11pt;
			}
			
			#checkout-form {
				margin-top: 0px;
				text-align: left;
				background-color: #f4e9bd;
				padding: 5px 0;
				margin-left: 160px;
			}
			
			#checkout-form input[type=submit] {
				position: relative;
				left: 600px;
				font-size: 8pt;
				font-weight: bold;
				padding: 4px;
				font-family: "Lucida Sans Unicode", "Lucida Grande", sans-serif;
			}
			
			div.error {
				text-align: center;
				width: 280px;
				color: #db391e;
				background-color: #eaeaae;
				font-family: arial;
				font-size: 9pt;
				padding: 5px;
				margin: 0 auto;
				font-weight: bold;
			}
		</style>
	</head>
	<body>
	<div id="wrapper">
	
		<?php dispatch("Banner"); ?>
		<?php include_once(dirname(__FILE__)."/leftbar.php"); ?>
		<div id="content-box">
		<?php if(isset($errors) && is_array($errors)) { ?>
			<div class="error">
			<?php
				foreach($errors as $field => $msg) {
					print $msg . "<br/>";
				} 
			?>
			</div>
		<?php } ?>
		<div id="cart-box">
			<table class="cart-table">
				<caption>Shopping cart</caption>
				<thead>
				<tr class="cart-column-header">
					<th></th>
					<th>Item</th>
					<th>Quantity</th>
					<th>Price</th>
				</tr>
				</thead>
				<tbody>
				<?php
				global $paths;
				$o = "";
				$totalPrice = 0;
				foreach($cartItems as $i => $cartItem) {
					$number = $i + 1;
					$name = ucwords(strtolower($cartItem['name']));
					$quantityPrice = $cartItem['price'] * $cartItem['quantity'];
					$totalPrice += $quantityPrice;
					$quantityPrice = number_format($quantityPrice, 2, '.', '');
					
					$o .= <<<EOS
					<tr class="cart-item">
						<td class="cart-item-number">{$number}</td>
						<td class="cart-item-name">
							<table>
							<tr>
								<td class="cart-item-name-picture"><img src="{$cartItem['image_default_path']}"/></td>
								<td class="cart-item-name-info">
									<div class="cart-item-name-name"><a href="{$paths['REL_ROOT_URL']}/items/{$cartItem['seo_name']}">{$name}</a></div>
									<div class="cart-item-name-productid">{$cartItem['product_id']}</div>
								</td>
							</tr>
							</table>
						</td>
						<td class="cart-item-quantity">
							<a style="color: #380000; text-decoration: none;" href="{$paths['REL_ROOT_URL']}/cart?do=remove&itemId={$cartItem['id']}" style="text-decoration: none;">[-]</a>
							{$cartItem['quantity']}
							<a style="color: #380000; text-decoration: none;" href="{$paths['REL_ROOT_URL']}/cart?do=add&itemId={$cartItem['id']}" style="text-decoration: none;">[+]</a>
						</td>
						<td class="cart-item-price">{$cartItem['currency_prefix']} {$quantityPrice}</td>
					</tr>
EOS;
				}
				
				if(count($cartItems) == 0) {
					$o .= <<<EOS
					<tr class="cart-empty">
						<td colspan="4">Cart is empty.</td>
					</tr>
EOS;
				}
				
				$totalPrice = number_format($totalPrice, 2, '.', '');
				$o .= <<<EOS
				<tr class="cart-total">
					<td colspan="3" class="cart-total-label">Total</td>
					<td class="cart-total-price">{$currencyPrefix} {$totalPrice}</td>
				</tr>
EOS;
				print $o;
				
				?>
				
				</tbody>
			</table>
		</div>
		<div id="checkout-form">
			<form action="<?php print $paths['REL_ROOT_URL']; ?>/cart/" method="post">
				<input name="checkout" type="submit" value="Checkout" />
				<!--
				<input name="checkout" type="image" src="https://www.paypal.com/en_US/i/btn/btn_xpressCheckout.gif" value="paypalexpress" style="text-align: left; margin-right:7px;" />
				<input name="checkout" type="image" src="{$paths['REL_ROOT_URL']}/webapp/images/2checkoutbutton.png" value="2checkout" style="text-align: left; margin-right:7px;" />
				-->
			</form>
		</div>
		</div>
		<?php include_once(dirname(__FILE__)."/footer.php"); ?>
	</div>
	</body>
</html>