<?php
	
	date_default_timezone_set("Europe/Belgrade");
	
	$urlControllerMap = array(
		"/" => "StoreFront",
		"404" => "StoreFront",
		"banner" => "Banner",
		"catalog" => "Catalog",
		"cart" => "Cart",
		"delivery" => "Delivery",
		"paypal-return" => "PaypalCheckoutReturn",
		"paypal-cancel" => "PaypalCheckoutCancel",
		"2checkout-receipt" => "TwoCheckoutReceipt",
		"2checkout-notifications" => "TwoCheckoutNotifications",
		"payment-success" => "PaymentSuccess",
		"payment-failed" => "PaymentFailed",
		"test-email" => "TestEmail",
		"view-order-email" => "ViewOrderEmail",
		"item-dispatchers" => "ItemDispatchers",
		"catalog-dispatchers" => "CatalogDispatchers"
	);
	
	$urlViewMap = array(
		"info" => "info/info.php",
		"contact-us" => "contactus.php",
		"about-us" => "aboutus.php",
		"info/delivery" => "info/delivery.php",
		"info/payment" => "info/payment.php"
	);
	
	
	$orderEmail = array(
		'from' => 'KosovaFlowers.com <info@kosovaflowers.com>',
		'replyTo' => 'Kosova Flowers Customer Support <support@kosovaflowers.com>',
		'bcc' => 'orders@kosovaflowers.com',
		'subject' => 'Thank you for your order with Kosova Flowers'
	);
	
	$notificationsLog = array(
		'filename' => ROOT_PATH."/notifications.log"
	);

?>