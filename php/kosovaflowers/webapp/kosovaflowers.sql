
-- Flowers


INSERT INTO catalog (id, name, seo_name) VALUES (80, 'Store Front', 'storefront');

INSERT INTO catalog (id, name, seo_name) VALUES (100, 'Flowers', 'flowers');

INSERT INTO catalog (id, name, seo_name) VALUES (110, 'Roses', 'roses');
INSERT INTO catalog (id, name, seo_name) VALUES (111, 'Daisies', 'daisies');
INSERT INTO catalog (id, name, seo_name) VALUES (112, 'Calla', 'calla');
INSERT INTO catalog (id, name, seo_name) VALUES (119, 'Mixed', 'mixed');

INSERT INTO catalog_entry (catalog_id, item_id) VALUES (100, 100);
INSERT INTO catalog_entry (catalog_id, item_id) VALUES (110, 100);
INSERT INTO item (id, product_id) VALUES (100, 'FL-RE-RS-0010');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (100, 'en', 'seo_name', '1-dozen-red-roses');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (100, 'en', 'image_default', 'FLRDRS2112.jpg');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (100, 'en', 'sku', 'FLRDRS2112');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (100, 'en', 'name', 'one dozen red roses');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (100, 'en', 'short_description', 'One dozen gorgeous long-stem red roses');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (100, 'en', 'long_description', 'Boxed or wrapped depending on availability and ready for the recipient to arrange in his or her own vase.');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (100, 'sh', 'seo_name', '1-dozen-red-roses');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (100, 'sh', 'image_default', 'FLRDRS2112.jpg');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (100, 'sh', 'name', 'tuf� lulesh t� kuqe u rrit');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (100, 'sh', 'short_description', '12 buron me tr�ndafila t� kuq t� bukur lucious p�rsosur p�r �do rast romantike.');
INSERT INTO item_price (item_id, currency_code, price) VALUES (100, 'eur', 34.99);

INSERT INTO catalog_entry (catalog_id, item_id) VALUES (100, 101);
INSERT INTO catalog_entry (catalog_id, item_id) VALUES (110, 101);
INSERT INTO item (id, product_id) VALUES (101, 'FL-WH-RS-0010');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (101, 'en', 'seo_name', '1-dozen-white-rose-bouquet');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (101, 'en', 'image_default', 'white_rose_bouquet.jpg');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (101, 'en', 'sku', 'FLWHRS2010');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (101, 'en', 'name', 'one dozen white rose bouquet');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (101, 'en', 'short_description', 'One dozen beautiful long-stem white roses');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (101, 'en', 'long_description', 'One dozen beautiful long-stem white roses tastefully arranged with fresh foliage. Simply elegant! Fresh and pure, white roses represent all that is innocent, truthful and humble.');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (101, 'sh', 'seo_name', '1-dozen-white-rose-bouquet');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (101, 'sh', 'image_default', 'white_rose_bouquet.jpg');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (101, 'sh', 'name', 'i bardh� u ngrit tuf� lulesh');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (101, 'sh', 'short_description', '12 buron me tr�ndafila t� bukur lucious e bardh� t� p�rsosur p�r �do rast romantike.');
INSERT INTO item_price (item_id, currency_code, price) VALUES (101, 'eur', 34.99);


INSERT INTO catalog_entry (catalog_id, item_id) VALUES (100, 102);
INSERT INTO catalog_entry (catalog_id, item_id) VALUES (110, 102);
INSERT INTO item (id, product_id) VALUES (102, 'FL-YE-RS-0010');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (102, 'en', 'seo_name', 'yellow-rose-bouquet');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (102, 'en', 'image_default', 'yellow_rose_bouquet.jpg');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (102, 'en', 'sku', 'FLYERS2010');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (102, 'en', 'name', 'one dozen yellow rose bouquet');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (102, 'en', 'short_description', 'Twelve premium yellow roses');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (102, 'en', 'long_description', 'Friendship, love, warmth, and remembrance: yellow roses offer a little bit of everything and are a special way to express affection. With their bold yellow color and rich fragrance, the roses make a memorable gift for any special occasion.');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (102, 'sh', 'seo_name', 'yellow-rose-bouquet');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (102, 'sh', 'image_default', 'yellow_rose_bouquet.jpg');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (102, 'sh', 'name', 'verdh� u rrit tuf� lulesh');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (102, 'sh', 'short_description', '12 stems of beautiful lucious yellow roses perfect for any romantic occasion.');
INSERT INTO item_price (item_id, currency_code, price) VALUES (102, 'eur', 34.99);


INSERT INTO catalog_entry (catalog_id, item_id) VALUES (100, 103);
INSERT INTO catalog_entry (catalog_id, item_id) VALUES (110, 103);
INSERT INTO item (id, product_id) VALUES (103, 'FL-PI-RS-0010');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (103, 'en', 'seo_name', '1-dozen-pink-rose-bouquet');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (103, 'en', 'image_default', 'FLPKRS2112.jpg');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (103, 'en', 'sku', 'FLPKRS2112');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (103, 'en', 'name', 'one dozen pink rose bouquet');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (103, 'en', 'short_description', 'One dozen beautiful long-stem pink roses');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (103, 'en', 'long_description', 'Sweet and joyful, blushing pink roses are often associated with grace, innocence and happiness. A bouquet of pink roses is a welcome gift for any special woman in your life.');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (103, 'sh', 'seo_name', 'pink-rose-bouquet');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (103, 'sh', 'image_default', 'pink_rose_bouquet.jpg');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (103, 'sh', 'name', 'pink rose tuf� lulesh');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (103, 'sh', 'short_description', '12 buron me tr�ndafila t� bukur lucious roz� t� p�rsosur p�r �do rast romantike.');
INSERT INTO item_price (item_id, currency_code, price) VALUES (103, 'eur', 34.99);


INSERT INTO item (id, product_id) VALUES (104, 'FL-YE-MX-1007');
INSERT INTO catalog_entry (catalog_id, item_id) VALUES (100, 104);
INSERT INTO catalog_entry (catalog_id, item_id) VALUES (119, 104);
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (104, 'en', 'seo_name', 'yellow-flower-medley');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (104, 'en', 'image_default', 'FLYEMX1007.jpg');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (104, 'en', 'sku', 'FLYEMX1007');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (104, 'en', 'name', 'Yellow Flower Medley');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (104, 'en', 'short_description', 'Yellow roses and flowers with greens and a dash of white.');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (104, 'en', 'long_description', 'Make any day happier with this modern summer flower arrangement! A stylish summer birthday gift or posh pick-me-up for any day of the week.');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (104, 'sh', 'seo_name', 'yellow-flower-medley');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (104, 'sh', 'image_default', 'FLYEMX1007.jpg');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (104, 'sh', 'name', 'pink rose tuf� lulesh');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (104, 'sh', 'short_description', '12 buron me tr�ndafila t� bukur lucious roz� t� p�rsosur p�r �do rast romantike.');
INSERT INTO item_price (item_id, currency_code, price) VALUES (104, 'eur', 28.00);

INSERT INTO item (id, product_id) VALUES (105, 'FL-WY-DR-2118');
INSERT INTO catalog_entry (catalog_id, item_id) VALUES (100, 105);
INSERT INTO catalog_entry (catalog_id, item_id) VALUES (110, 105);
INSERT INTO catalog_entry (catalog_id, item_id) VALUES (119, 105);
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (105, 'en', 'seo_name', 'rose-daisy-bouquet');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (105, 'en', 'image_default', 'FLWYDR2118.jpg');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (105, 'en', 'sku', 'FLWYDR2118');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (105, 'en', 'name', 'Rose Daisy Bouquet');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (105, 'en', 'short_description', '18 yellow and white roses and daisies.');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (105, 'en', 'long_description', 'When you\'re looking to make someone smile, this happy face mug of roses and daisies is tops. Sure to cheer up everyone from a beloved wife to a busy boss.');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (105, 'sh', 'seo_name', 'rose-daisy-bouquet');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (105, 'sh', 'image_default', 'FLWYDR2118.jpg');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (105, 'sh', 'name', 'pink rose tuf� lulesh');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (105, 'sh', 'short_description', '12 buron me tr�ndafila t� bukur lucious roz� t� p�rsosur p�r �do rast romantike.');
INSERT INTO item_price (item_id, currency_code, price) VALUES (105, 'eur', 41.99);

INSERT INTO item (id, product_id) VALUES (106, 'FL-WH-RS-2120');
INSERT INTO catalog_entry (catalog_id, item_id) VALUES (100, 106);
INSERT INTO catalog_entry (catalog_id, item_id) VALUES (110, 106);
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (106, 'en', 'seo_name', '2-dozen-white-rose-bouquet');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (106, 'en', 'image_default', 'FLWHRS2120.jpg');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (106, 'en', 'sku', 'FLWHRS2120');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (106, 'en', 'name', 'Two Dozen White Rose Bouquet');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (106, 'en', 'short_description', 'Two dozen beautiful long-stem white roses bouquet.');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (106, 'en', 'long_description', 'Simply elegant! Fresh and pure, white roses represent all that is innocent, truthful and humble.');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (106, 'sh', 'seo_name', '2-dozen-white-rose-bouquet');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (106, 'sh', 'image_default', 'FLWHRS2120.jpg');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (106, 'sh', 'name', 'pink rose tuf� lulesh');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (106, 'sh', 'short_description', '12 buron me tr�ndafila t� bukur lucious roz� t� p�rsosur p�r �do rast romantike.');
INSERT INTO item_price (item_id, currency_code, price) VALUES (106, 'eur', 59.99);

INSERT INTO item (id, product_id) VALUES (107, 'FL-WH-DS-2112');
INSERT INTO catalog_entry (catalog_id, item_id) VALUES (100, 107);
INSERT INTO catalog_entry (catalog_id, item_id) VALUES (111, 107);
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (107, 'en', 'seo_name', '1-dozen-white-daisy-bouquet');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (107, 'en', 'image_default', 'FLWHDS2112.jpg');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (107, 'en', 'sku', 'FLWHDS2112');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (107, 'en', 'name', 'One Dozen White Daisy Bouquet');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (107, 'en', 'short_description', 'One dozen pure white gerbera daisies.');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (107, 'en', 'long_description', 'White Gerbera Daisies are popular for weddings and table centerpieces and add a Spring look to arrangements. Their open petals evoke a sense of playfulness and happiness.');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (107, 'sh', 'seo_name', '1-dozen-white-daisy-bouquet');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (107, 'sh', 'image_default', 'FLWHDS2112.jpg');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (107, 'sh', 'name', 'pink rose tuf� lulesh');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (107, 'sh', 'short_description', '12 buron me tr�ndafila t� bukur lucious roz� t� p�rsosur p�r �do rast romantike.');
INSERT INTO item_price (item_id, currency_code, price) VALUES (107, 'eur', 32.00);

INSERT INTO item (id, product_id) VALUES (108, 'FL-WH-DR-2112');
INSERT INTO catalog_entry (catalog_id, item_id) VALUES (100, 108);
INSERT INTO catalog_entry (catalog_id, item_id) VALUES (110, 108);
INSERT INTO catalog_entry (catalog_id, item_id) VALUES (111, 108);
INSERT INTO catalog_entry (catalog_id, item_id) VALUES (119, 108);
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (108, 'en', 'seo_name', 'white-rose-daisy-mini-bouquet');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (108, 'en', 'image_default', 'FLWHDR2112.jpg');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (108, 'en', 'sku', 'FLWHDR2112');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (108, 'en', 'name', 'Rose Daisy Wedding Bouquet');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (108, 'en', 'short_description', 'Dozen white roses and daisies.');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (108, 'en', 'long_description', 'Perfect arrangement to hand to your loved one, or used for weddings. Has a touch of folliage.');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (108, 'sh', 'seo_name', 'white-rose-daisy-mini-bouquet');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (108, 'sh', 'image_default', 'FLWHDR2112.jpg');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (108, 'sh', 'name', 'pink rose tuf� lulesh');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (108, 'sh', 'short_description', '12 buron me tr�ndafila t� bukur lucious roz� t� p�rsosur p�r �do rast romantike.');
INSERT INTO item_price (item_id, currency_code, price) VALUES (108, 'eur', 28.00);

INSERT INTO item (id, product_id) VALUES (109, 'FL-WH-CL-2116');
INSERT INTO catalog_entry (catalog_id, item_id) VALUES (100, 109);
INSERT INTO catalog_entry (catalog_id, item_id) VALUES (112, 109);
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (109, 'en', 'seo_name', '16-white-calla-bouquet');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (109, 'en', 'image_default', 'FLWHCL2116.jpg');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (109, 'en', 'sku', 'FLWHCL2116');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (109, 'en', 'name', 'White Calla Bouquet');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (109, 'en', 'short_description', '16 beautiful white calla flowers.');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (109, 'en', 'long_description', 'An extraordinary display of these exquisite white blooms. Gorgeous and bright, our finest full-sized white calla lilies capture the essence of beauty.');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (109, 'sh', 'seo_name', '16-white-calla-bouquet');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (109, 'sh', 'image_default', 'FLWHCL2116.jpg');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (109, 'sh', 'name', 'pink rose tuf� lulesh');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (109, 'sh', 'short_description', '12 buron me tr�ndafila t� bukur lucious roz� t� p�rsosur p�r �do rast romantike.');
INSERT INTO item_price (item_id, currency_code, price) VALUES (109, 'eur', 41.99);

INSERT INTO item (id, product_id) VALUES (110, 'FL-RW-RS-2112');
INSERT INTO catalog_entry (catalog_id, item_id) VALUES (100, 110);
INSERT INTO catalog_entry (catalog_id, item_id) VALUES (110, 110);
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (110, 'en', 'seo_name', '12-red-white-rose-bouquest');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (110, 'en', 'image_default', 'FLRWRS2112.jpg');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (110, 'en', 'sku', 'FLRWRS2112');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (110, 'en', 'name', '12 White Red Rose Bouquet');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (110, 'en', 'short_description', 'Dozen stunning red and white roses.');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (110, 'en', 'long_description', 'Style and luxury. A natural beauty is presented through this collection of a dozen red and white roses arranged delicately with green fillers in a glass vase that will fill your loved ones day with passionate allure.');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (110, 'sh', 'seo_name', '12-red-white-rose-bouquest');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (110, 'sh', 'image_default', 'FLRWRS2112.jpg');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (110, 'sh', 'name', 'pink rose tuf� lulesh');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (110, 'sh', 'short_description', '12 buron me tr�ndafila t� bukur lucious roz� t� p�rsosur p�r �do rast romantike.');
INSERT INTO item_price (item_id, currency_code, price) VALUES (110, 'eur', 44.99);

-- Chocolates

INSERT INTO catalog (id, name, seo_name) VALUES (200, 'chocolates', 'chocolates');

INSERT INTO item (id, product_id) VALUES (201, 'CHO-100');
INSERT INTO catalog_entry (catalog_id, item_id) VALUES (200, 201);
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (201, 'en', 'seo_name', 'chocolate-general-assortment');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (201, 'en', 'image_default', 'chocolates/chocolate-general-assortment.png');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (201, 'en', 'sku', 'CHO100');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (201, 'en', 'name', 'Chocolate General Assortment');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (201, 'en', 'short_description', 'Assortment of dark, milk and white chocolates. 450g');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (201, 'en', 'long_description', 'A well-balanced and representative selection that fully illustrates Leonidas� wide range of Belgian Pralines. This assortment includes the Manon Blanc and Manon Caf� alongside a representative sampling of fresh butter creams, Ganaches with fresh cream, and Pralin�s (by far the most popular, always ideal for all gift-giving occasions!)');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (201, 'sh', 'seo_name', 'chocolate-general-assortment');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (201, 'sh', 'image_default', 'chocolates/chocolate-general-assortment.png');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (201, 'sh', 'name', 'pink rose tuf� lulesh');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (201, 'sh', 'short_description', '12 buron me tr�ndafila t� bukur lucious roz� t� p�rsosur p�r �do rast romantike.');
INSERT INTO item_price (item_id, currency_code, price) VALUES (201, 'eur', 20.99);

INSERT INTO item (id, product_id) VALUES (202, 'CHO-101');
INSERT INTO catalog_entry (catalog_id, item_id) VALUES (200, 202);
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (202, 'en', 'seo_name', 'fruit-marzipan');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (202, 'en', 'image_default', 'chocolates/fruit-marzipan.png');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (202, 'en', 'sku', 'CHO101');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (202, 'en', 'name', 'Fruit Marzipan');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (202, 'en', 'short_description', 'Assortment of citrus flavored chocolates. 450g');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (202, 'en', 'long_description', 'Well known and loved throughout Europe, Leonidas small marzipan fruits are now available here. The selection includes mixed shapes including oranges, apples, pears, strawberries, bananas and lemons. The 450g ballotin contains 18 pieces.');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (202, 'sh', 'seo_name', 'fruit-marzipan');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (202, 'sh', 'image_default', 'chocolates/fruit-marzipan.png');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (202, 'sh', 'name', 'pink rose tuf� lulesh');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (202, 'sh', 'short_description', '12 buron me tr�ndafila t� bukur lucious roz� t� p�rsosur p�r �do rast romantike.');
INSERT INTO item_price (item_id, currency_code, price) VALUES (202, 'eur', 20.99);

INSERT INTO item (id, product_id) VALUES (203, 'CHO-102');
INSERT INTO catalog_entry (catalog_id, item_id) VALUES (200, 203);
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (203, 'en', 'seo_name', 'giantina');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (203, 'en', 'image_default', 'chocolates/giantina.png');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (203, 'en', 'sku', 'CHO102');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (203, 'en', 'name', 'Giantina');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (203, 'en', 'short_description', 'Italian pralin� with bits of biscuit. 450g.');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (203, 'en', 'long_description', 'The same famous Italian pralin� as used in the Gianduja but with bits of biscuit that add a subtle crispiness. Ballotin contains all Giantina pieces.');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (203, 'sh', 'seo_name', 'giantina');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (203, 'sh', 'image_default', 'chocolates/giantina.png');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (203, 'sh', 'name', 'pink rose tuf� lulesh');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (203, 'sh', 'short_description', '12 buron me tr�ndafila t� bukur lucious roz� t� p�rsosur p�r �do rast romantike.');
INSERT INTO item_price (item_id, currency_code, price) VALUES (203, 'eur', 20.99);

INSERT INTO item (id, product_id) VALUES (204, 'CHO-103');
INSERT INTO catalog_entry (catalog_id, item_id) VALUES (200, 204);
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (204, 'en', 'seo_name', 'gianduja');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (204, 'en', 'image_default', 'chocolates/gianduja.png');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (204, 'en', 'sku', 'CHO103');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (204, 'en', 'name', 'Gianduja');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (204, 'en', 'short_description', 'Leonidas\' signature piece of pure almond & hazelnut pralin�. 450g.');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (204, 'en', 'long_description', 'Wrapped in gold foil, Gianduja is pure almond & hazelnut pralin�. Delicate and full of chocolate and hazelnut flavor, it has been a Leonidas\' signature piece for decades. This 450g ballotin is composed of entirely Gianduja pieces.');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (204, 'sh', 'seo_name', 'gianduja');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (204, 'sh', 'image_default', 'chocolates/gianduja.png');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (204, 'sh', 'name', 'pink rose tuf� lulesh');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (204, 'sh', 'short_description', '12 buron me tr�ndafila t� bukur lucious roz� t� p�rsosur p�r �do rast romantike.');
INSERT INTO item_price (item_id, currency_code, price) VALUES (204, 'eur', 20.99);

INSERT INTO item (id, product_id) VALUES (205, 'CHO-104');
INSERT INTO catalog_entry (catalog_id, item_id) VALUES (200, 205);
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (205, 'en', 'seo_name', 'manon-cafe-manon-blanc');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (205, 'en', 'image_default', 'chocolates/manon-cafe-manon-blanc.png');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (205, 'en', 'sku', 'CHO104');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (205, 'en', 'name', 'Manon Caf� & Manon Blanc');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (205, 'en', 'short_description', 'Light coffee-flavored, fresh butter cream filling with roasted hazelnut and white chocolate. 450g.');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (205, 'en', 'long_description', 'This is the very popular and often requested assortment that we featured in our traditional and eagerly awaited popular January "Whites Sale." these last few years. This assortment contains only the Manon Caf� and Manon Blanc pieces in about equal proportions.');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (205, 'sh', 'seo_name', 'manon-cafe-manon-blanc');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (205, 'sh', 'image_default', 'chocolates/manon-cafe-manon-blanc.png');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (205, 'sh', 'name', 'pink rose tuf� lulesh');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (205, 'sh', 'short_description', '12 buron me tr�ndafila t� bukur lucious roz� t� p�rsosur p�r �do rast romantike.');
INSERT INTO item_price (item_id, currency_code, price) VALUES (205, 'eur', 20.99);

INSERT INTO item (id, product_id) VALUES (206, 'CHO-105');
INSERT INTO catalog_entry (catalog_id, item_id) VALUES (200, 206);
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (206, 'en', 'seo_name', 'milk-chocolate-assortment');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (206, 'en', 'image_default', 'chocolates/milk-chocolate-assortment.png');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (206, 'en', 'sku', 'CHO105');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (206, 'en', 'name', 'Milk Chocolate Assortment');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (206, 'en', 'short_description', 'Well balanced assortment of milk chocolates. 450g.');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (206, 'en', 'long_description', 'For those with a sweeter tooth, a well balanced and representative selection of fresh butter creams, Ganaches with fresh cream, Pralin�s and Soft Caramel centers, all in milk chocolate covering.');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (206, 'sh', 'seo_name', 'milk-chocolate-assortment');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (206, 'sh', 'image_default', 'chocolates/milk-chocolate-assortment.png');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (206, 'sh', 'name', 'pink rose tuf� lulesh');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (206, 'sh', 'short_description', '12 buron me tr�ndafila t� bukur lucious roz� t� p�rsosur p�r �do rast romantike.');
INSERT INTO item_price (item_id, currency_code, price) VALUES (206, 'eur', 20.99);

INSERT INTO item (id, product_id) VALUES (207, 'CHO-106');
INSERT INTO catalog_entry (catalog_id, item_id) VALUES (200, 207);
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (207, 'en', 'seo_name', 'dark-chocolate-assortment');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (207, 'en', 'image_default', 'chocolates/dark-chocolate-assortment.png');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (207, 'en', 'sku', 'CHO106');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (207, 'en', 'name', 'Dark Chocolate Assortment');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (207, 'en', 'short_description', 'Purest bittersweet dark chocolate. 450g.');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (207, 'en', 'long_description', 'For those who truly enjoy the purest bittersweet chocolate taste, a well-balanced and representative selection of Leonidas� masterpieces in dark chocolate covering to dazzle the most discerning palate.');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (207, 'sh', 'seo_name', 'dark-chocolate-assortment');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (207, 'sh', 'image_default', 'chocolates/dark-chocolate-assortment.png');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (207, 'sh', 'name', 'pink rose tuf� lulesh');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (207, 'sh', 'short_description', '12 buron me tr�ndafila t� bukur lucious roz� t� p�rsosur p�r �do rast romantike.');
INSERT INTO item_price (item_id, currency_code, price) VALUES (207, 'eur', 20.99);

INSERT INTO item (id, product_id) VALUES (208, 'CHO-107');
INSERT INTO catalog_entry (catalog_id, item_id) VALUES (200, 208);
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (208, 'en', 'seo_name', 'chocolates-decorative');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (208, 'en', 'image_default', 'chocolates/chocolates-decorative.png');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (208, 'en', 'sku', 'CHO107');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (208, 'en', 'name', 'Chocolates Decorative');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (208, 'en', 'short_description', 'Assorted fillings in ivory, milk, and dark chocolate coverings. 450g.');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (208, 'en', 'long_description', 'Leonidas Chocolates Decorative Ballotins - perfectly decorated for the season or any occasion. The assortment includes our most popular pieces, including assorted fillings in ivory, milk, and dark chocolate coverings.');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (208, 'sh', 'seo_name', 'chocolates-decorative');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (208, 'sh', 'image_default', 'chocolates/chocolates-decorative.png');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (208, 'sh', 'name', 'pink rose tuf� lulesh');
INSERT INTO item_attr (item_id, language_code, name, value) VALUES (208, 'sh', 'short_description', '12 buron me tr�ndafila t� bukur lucious roz� t� p�rsosur p�r �do rast romantike.');
INSERT INTO item_price (item_id, currency_code, price) VALUES (208, 'eur', 20.99);

INSERT INTO location (name, locality, active) VALUES ('Pristina', 'CITY', 1);
INSERT INTO location_fee (location_name, currency_code, fee) VALUES ('Pristina', 'eur', 2.99);
INSERT INTO location (name, locality, active) VALUES ('Ferizaj', 'CITY', 1);
INSERT INTO location_fee (location_name, currency_code, fee) VALUES ('Ferizaj', 'eur', 10.99);