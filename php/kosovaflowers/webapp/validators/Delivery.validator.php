<?php

	require_once(__DIR__."/../../pp/pp.php");
	require_once(WEB_FRAMEWORK_PATH."/mvc/Validator.class.php");

	class Validator_Delivery extends Validator {
		public function validate(&$data) {
			$errors = array();
			$this->trimArray($data);
			@extract($data);
			
			if(isset($customerEmail)) {
				if(empty($customerEmail)) {
					$errors['customerEmail'] = "Please enter your email address.";
				}
				else if(!$this->validEmail($customerEmail)) {
					$errors['customerEmail'] = "Invalid email address.";
				}
			}

			if(isset($customerPhone)) {
				$customerPhone = str_replace(array(' ', '-','+'), "", $customerPhone);
				if(empty($customerPhone)) {
					$errors['customerPhone'] = "Please enter your phone number.";
				}
				else if(!$this->onlyDigits($customerPhone, 5, 16)) {
					$errors['customerPhone'] = "Invalid phone number.";
				}
				$data['customerPhone'] = $customerPhone;
			}
			
			if(empty($fullName))
				$errors['fullName'] = "Please enter the recipient's name.";
			if(empty($address1))
				$errors['address1'] = "Please enter recipient's address.";
				
			if(empty($zip)) {
				$errors['zip'] = "Please enter recipient's zip.";
			}
			else if(!$this->onlyDigits($zip, 5, 5)) {
				$errors['zip'] = "Invalid zip.";
			}
			
			if(empty($city))
				$errors['city'] = "Please enter recipient's city.";
			
			if(isset($phone)) {
				$phone = str_replace(array(' ', '-','+'), "", $phone);
				if(empty($phone)) {
					$errors['phone'] = "Please enter recipient's phone number.";
				}
				else if(!$this->onlyDigits($phone, 5, 16)) {
					$errors['phone'] = "Invalid phone number.";
				}
				$data['phone'] = $phone;
			}
			
			if(empty($deliveryDate))
				$errors['deliveryDate'] = "Select a delivery date.";
		
			if(isset($data['deliveryDate'])) {
				$date1 = new DateTime($data['deliveryDate2']);
				$date2 = new DateTime("+2 day 00:00:00");
				if($date1 < $date2) {
					$data['deliveryDate'] = $date2->format("Y-m-d");
					$errors['deliveryDate'] = "Delivery date changed. Has to be at least 2 days from today.";
				}
				
				if(strtolower(date_format($date1, "D")) == "sun") {
					$errors['deliveryDate'] = "Currently, we do not deliver on Sundays.";
				}
			}
			else {
				$date1 = new DateTime("+2 day 00:00:00");
				$data['deliveryDate'] = $date1->format("Y-m-d");
			}
			
			if(!isset($paymentType)) {
				$errors['paymentType'] = "Please select a payment method.";
			}
			
			return $errors;
		}
	}
