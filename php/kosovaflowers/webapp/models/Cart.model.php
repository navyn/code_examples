<?php
	
	require_once(dirname(__FILE__)."/../../pp/pp.php");
	require_once(WEB_FRAMEWORK_PATH."/Database.class.php");
	require_once(MODEL_PATH."/Item.model.php");
	
	class Model_Cart {
		
		const STATUS_OPEN = 'OPEN';
		const STATUS_EXPIRED = 'EXPIRED';
		const STATUS_CONVERSION = 'CONVERSION';
		
		public static function getCartId(&$session) {
			$cartId = 0;
			if(isset($session['cart_id'])) {
				$cartId = $session['cart_id'];
			}
			return $cartId;
		}
		
		public static function setCartId(&$session, $cartId) {
			$session['cart_id'] = $cartId;
		}
		
		public static function cartConversion($db, &$session) {
			$cartId = self::getCartId($session);
			self::setStatus($db, $cartId, self::STATUS_CONVERSION);
			unset($session['cart_id']);
		}
		
		public static function setStatus($db, $cartId, $status) {
			$q1 = "update cart set status = '{$status}' where id = {$cartId}";
			$r = $db->exec($q1);
		}
		
		public static function getStatus($db, $cartId) {
			$status = null;
			$q1 = "select status from cart where id = {$cartId}";
			$rs = $db->exec($q1);
			if($rs->hasNext()) {
				$row = $rs->getNextRow();
				$status = $row['status'];
			}
			return $status;
		}
		
		public static function cartWritten($db, $cartId) {
			$q1 = "select * from cart where id = {$cartId}";
			$rs1 = $db->exec($q1);
			$written = false;
			if($rs1) {
				if($rs1->hasNext()) {
					$written = true;
				}
				else {
					$written = false;
				}
			}
			return $written;
		}
		
		private static function nextCartId($db) {
			$nextCartId = 1000;
			$q1 = "select distinct max(id) + 1 as next_cart_id from cart";
			//print $q1 . "<br/>";
			$rs1 = $db->exec($q1);
			if($rs1)
			if($rs1->hasNext()) {
				$obj = $rs1->getNextObject();
				$nextCartId = $obj->next_cart_id;
				if(empty($nextCartId)) {
					$nextCartId = 1001;
				}
			}
			return $nextCartId;
		}
		
		public static function addToCart($db, &$session, $itemId, $quantity) {
			$cartId = self::getCartId($session);
			$q1 = "select quantity from cart where id = {$cartId} and item_id = {$itemId}";
			//print $q1 . "<br/>";
			$rs1 = $db->exec($q1);
			if($rs1)
			if($rs1->hasNext()) {
				$obj = $rs1->getNextObject();
				$quantity += $obj->quantity;
				$q2 = "update cart set quantity = {$quantity}, last_update = CURRENT_TIMESTAMP where id = {$cartId} and item_id = {$itemId}";
				//print $q2 . "<br/>";
				$db->exec($q2);
			}
			else {
				if($cartId == 0) {
					$cartId = self::nextCartId($db);
					self::setCartId($session, $cartId);
				}
				$q3 = "insert into cart (id, item_id, quantity) values ({$cartId}, {$itemId}, {$quantity})";
				//print $q3 . "<br/>";
				$db->exec($q3);
			}
			
		}
		
		public static function removeFromCart($db, $session, $itemId, $quantity) {
			$cartId = self::getCartId($session);
			$q1 = "select quantity from cart where id = {$cartId} and item_id = {$itemId}";
			//print $q1 . "<br/>";
			$rs1 = $db->exec($q1);
			if($rs1->hasNext()) {
				$obj = $rs1->getNextObject();
				$quantity = $obj->quantity - $quantity;
				if($quantity > 0) {
					$q2 = "update cart set quantity = {$quantity}, last_update = CURRENT_TIMESTAMP where id = {$cartId} and item_id = {$itemId}";
					//print $q2 . "<br/>";
					$db->exec($q2);
				}
				else {
					$q3 = "delete from cart where id = {$cartId} and item_id = {$itemId}";
					//print $q3 . "<br/>";
					$db->exec($q3);
				}
			}
		}
		
		public static function getCartItems($db, $session, $languageCode, $currencyCode) {
			$cartId = self::getCartId($session);
			$cartItems = array();
			if($cartId) {
				$q2 = "select i.id as item_id, i.product_id as product_id, group_concat(ia.name separator '|') as item_attr_names, group_concat(ia.value separator '|') as item_attr_values, ip.price as price, cur.prefix as currency_prefix, c.quantity as quantity, c.last_update as last_update, cur.code as currency_code from cart c, item i, item_attr ia, item_price ip, currency cur where i.id = ia.item_id and i.id = ip.item_id and ip.currency_code = cur.code and i.id = c.item_id and i.id in (select item_id from cart where c.id = {$cartId}) and ia.language_code = '{$languageCode}' and ip.currency_code = '{$currencyCode}' group by i.id order by c.last_update desc";
				$rs2 = $db->exec($q2);
				if($rs2->hasNext()) {
					$objects = $rs2->getAllObjects();
					$cartItems = Model_Cart::objectsToCartItems($objects);
				}
			}
			return $cartItems;
		}
	
		public static function countCartItems($db, $session) {
			$cartId = self::getCartId($session);
			$cartItemCount = 0;
			if($cartId) {
				$q2 = "select sum(c.quantity) as cart_item_count from cart c where c.id = {$cartId}";
				$rs2 = $db->exec($q2);
				if($rs2->hasNext()) {
					$object = $rs2->getNextObject();
					$cartItemCount = $object->cart_item_count;
				}
			}
			return $cartItemCount;
		}
	
		public static function objectsToCartItems($objects) {
			$cartItems = array();
			foreach($objects as $obj) {
				$attrNames = explode("|", $obj->item_attr_names);
				$attrValues = explode("|", $obj->item_attr_values);
				$itemAttrs = array_combine($attrNames, $attrValues);
				$imageDefaultPath = PUBLIC_URL.URL_DIRECTORY_SEPARATOR."item_images".URL_DIRECTORY_SEPARATOR.$itemAttrs['image_default'];
				$cartItem = array(
					"id" => $obj->item_id,
					"product_id" => $obj->product_id,
					"price" => $obj->price,
					"currency_prefix" => $obj->currency_prefix,
					"currency_code" => $obj->currency_code,
					"image_default_path" => $imageDefaultPath,
					"quantity" => $obj->quantity,
					"last_update" => $obj->last_update
				);
				$cartItem = array_merge($cartItem, $itemAttrs);
				$cartItems[] = $cartItem;
			}
			return $cartItems;
		}
		
	}

?>