<?php

	require_once(dirname(__FILE__)."/../../pp/pp.php");
	
	class Model_Item {
		
		public static function objectsToItems($objects) {
			$items = array();
			foreach($objects as $obj) {
				$attrNames = explode("|", $obj->item_attr_names);
				$attrValues = explode("|", $obj->item_attr_values);
				$itemAttrs = array_combine($attrNames, $attrValues);
				$imageDefaultPath = PUBLIC_URL.URL_DIRECTORY_SEPARATOR."item_images".URL_DIRECTORY_SEPARATOR.$itemAttrs['image_default'];
				$item = array(
					"id" => $obj->item_id,
					"product_id" => $obj->product_id,
					"price" => $obj->price,
					"currency_prefix" => $obj->currency_prefix,
					"image_default_path" => $imageDefaultPath,
					"seo_name" => $obj->seo_name
				);
				$item = array_merge($item, $itemAttrs);
				$items[] = $item;
			}
			return $items;
		}
		
		public static function getSeoNames($db) {
			$q1 = "SELECT value FROM item i INNER JOIN item_attr ia ON i.id = ia.item_id WHERE name = 'seo_name'";
			$rs = $db->exec($q1);
			$seoNames = array();
			if($rs->hasNext()) {
				$seoNames = $rs->getAllRows();
			}
			return $seoNames;
		}
	}

?>