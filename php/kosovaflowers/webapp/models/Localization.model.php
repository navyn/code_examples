<?php

	class Model_Localization {
		public static function setLanguageCode(&$session, $languageCode) {
			$session['language_code'] = $languageCode;
		}
		
		public static function getLanguageCode($session) {
			$languageCode = 'en';
			if(isset($session['language_code'])) {
				$languageCode = $session['language_code'];
			}
			return $languageCode;
		}
		
		public static function setCurrencyCode(&$session, $currencyCode) {
			$session['currency_code'] = $currencyCode;
		}
		
		public static function getCurrencyCode($session) {
			$currencyCode = 'eur';
			if(isset($session['currency_code'])) {
				$currencyCode = $session['currency_code'];
			}
			return $currencyCode;
		}
		
		public static function getCurrencyPrefix($db, $session) {
			$prefix = "";
			$currencyCode = self::getCurrencyCode($session);
			$q1 = "SELECT prefix FROM currency WHERE code = '{$currencyCode}'";
			$rs = $db->exec($q1);
			if($rs->hasNext()) {
				$r = $rs->getNextRow();
				$prefix = $r['prefix'];
			}
			return $prefix;
		}
	}
?>