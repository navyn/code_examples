<?php

	require_once(dirname(__FILE__)."/../../pp/pp.php");
	require_once(WEB_FRAMEWORK_PATH."/lib/Http.lib.php");
	require_once(WEB_FRAMEWORK_PATH."/mvc/ViewCode.class.php");

	class PaypalStandard {
		const SANDBOX_UPLOAD_URL = 'https://www.paypal.com/cgi-bin/webscr';
		const SANDBOX_EXPRESS_CHECKOUT_URL = 
			'https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token=';
	
		public static function upload($business, $cartItems, $currencyCode, $returnUrl, $cancelUrl) {
			$postData = array(
				'cmd' => "_cart",
				'upload' => 1,
				'business' => $business,
				'currency_code' => strtoupper($currencyCode),
				'return' => $returnUrl,
				'cancel_return' => $cancelUrl
			);
			
			$i = 0;
			foreach($cartItems as $cartItem) {
				$i++;
				$postData["item_name_{$i}"] = $cartItem['name'];
				$postData["amount_{$i}"] = $cartItem['price'] * $cartItem['quantity'];
			}
			
			$response = Http::postForm(self::SANDBOX_UPLOAD_URL, $postData);
			$responseData = explode("\r\n\r\n", $response);
			$header = explode("\r\n", $responseData[0]);
			$redirectUrl = Http::getRedirectUrl($responseData[0]);
			$cookieDough = Http::getHeaderValue($responseData[0], "Set-Cookie");
			$cookieData = explode("; ", $cookieDough);
			foreach($cookieData as $i => $e) {
				$e = strtolower($e);
				if(
					$e == "httponly" ||
					$e == "secure" ||
					strpos($e, "path=") !== false ||
					strpos($e, "domain=") !== false ||
					strpos($e, "expires=") !== false
				) {
					unset($cookieData[$i]);
				}
			}
			$cookie = implode("; ", $cookieData);

			$headers = "Cookie: {$cookie}";
			$response = Http::get($redirectUrl, $headers);
			$responseData = explode("\r\n\r\n", $response);
			
			//$headers = "Cookie: {$cookie}\r\n";
			//$headers .= 'User-Agent: Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.13) Gecko/20101203 Firefox/3.6.13\r\n';
			//$headers .= 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8\r\n';
			//$headers .= 'Accept-Language: en-us,en;q=0.5\r\n';
			//$headers .= 'Accept-Encoding: gzip,deflate\r\n';
			//$headers .= 'Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7';

			$view = new ViewCode($responseData[1]);
			return $view;
		}
	}

?>