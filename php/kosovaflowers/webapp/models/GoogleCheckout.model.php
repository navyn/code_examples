<?php
	require_once(dirname(__FILE__)."/../../pp/pp.php");
	require_once(WEB_FRAMEWORK_PATH."/mvc/View.class.php");
	require_once(WEB_FRAMEWORK_PATH."/Database.class.php");
	require_once(WEB_FRAMEWORK_PATH."/lib/Http.lib.php");
	
	class GoogleCheckout {
		public static function hello() {
			$postData = array(
				'_type' => "hello"
			);
			
			$merchantId = "247355945570368";
			$merchantKey = "ANtN7eoWvyclP40apMWzgg";
			$authStr = base64_encode($merchantId.":".$merchantKey);
			$header = "Host: sandbox.google.com\r\nAuthorization: Basic {$authStr}\r\nUser-Agent: curl/7.21.3 (i386-pc-win32) libcurl/7.21.3 OpenSSL/1.0.0a zlib/1.2.5 libssh2/1.2.6\r\nContent-Type: application/x-www-form-urlencoded\r\nContent-Length: 11\r\nAccept: */*";
			
			$response = Http::request2("https://sandbox.google.com/checkout/api/checkout/v2/requestForm/Merchant/{$merchantId}", "POST", $header, "_type=hello");
			var_dump(Http::getPostData($response));
			exit(1);
		}
	
		public static function checkout($cartItems) {
			$postData = array(
				'_type' => "checkout-shopping-cart"
			);
			
			foreach($cartItems as $i => $cartItem) {
				$itemNo = $i + 1;
				$postData['item_name_'.$itemNo] = $cartItem['name'];
				$postData['item_description_'.$itemNo] = $cartItem['short_description'];
				$postData['item_quantity_'.$itemNo] = $cartItem['quantity'];
				$postData['item_price_'.$itemNo] = $cartItem['price'];
				//$postData['item_currency_'.$itemNo] = strtoupper($cartItem['currency_code']);
				$postData['item_currency_'.$itemNo] = "USD";
			}
			
			$merchantId = "247355945570368";
			$merchantKey = "ANtN7eoWvyclP40apMWzgg";
			$authStr = base64_encode($merchantId.":".$merchantKey);
			$header = "Authorization: Basic {$authStr}\r\nContent-Type: application/xml;charset=UTF-8\r\nAccept: application/xml;charset=UTF-8";
			
			$response = Http::postForm("https://sandbox.google.com/checkout/api/checkout/v2/merchantCheckoutForm/Merchant/{$merchantId}", $postData, $header);
			$responsePostData = Http::getPostData($response);
			//print_r($responsePostData);
			
			if(isset($responsePostData['_type']) && $responsePostData['_type'] == "checkout-redirect") {
				$serialNumber = $responsePostData['serial-number'];
				$redirectUrl = $responsePostData['redirect-url'];
				View::redirect($redirectUrl);		
			}
		}
	}

?>