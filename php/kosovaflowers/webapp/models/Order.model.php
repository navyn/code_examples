<?php

	require_once(__DIR__."/../../pp/pp.php");
	require_once(WEB_FRAMEWORK_PATH."/Ex.exception.php");
	require_once(WEB_FRAMEWORK_PATH."/actions/Insert.class.php");
	require_once(MODEL_PATH."/Customer.model.php");
	require_once(MODEL_PATH."/Delivery.model.php");
	require_once(MODEL_PATH."/Payment.model.php");
	require_once(MODEL_PATH."/Cart.model.php");
	require_once(ROOT_PATH."/objschema.php");
	
	class Model_Order {
		public static function createNewOrder($db, &$session, $customer, $delivery, $cartItems, $deliveryFee) {
			$customerId = Model_Customer::addCustomer($db, $customer);
			$orderNumber = null;
			if(isset($session['orderNumber'])) {
				$orderNumber = $session['orderNumber'];
				self::updateOrderCustomerId($db, $orderNumber, $customerId);
				self::clearOrderItems($db, $orderNumber);
				Model_Delivery::deleteDelivery($db, $orderNumber);
				self::clearOrderAttrs($db, $orderNumber);
			}
			else {
				$order = new RelObj_orders();
				$order->customer_id = $customerId;
				$orderNumber = self::addOrder($db, $order);
				$session['orderNumber'] = $orderNumber;
			}
			self::addOrderItems($db, $orderNumber, $cartItems);
			self::addOrderAttr($db, $orderNumber, 'delivery_fee', $deliveryFee);
			$delivery->order_number = $orderNumber;
			$deliveryId = Model_Delivery::addDelivery($db, $delivery);
			return $orderNumber;
		}
		
		public static function pay($db, $orderNumber, $paymentTypeCode, $status = 'PENDING', $transactionId = '0') {
			$payment = new RelObj_payment();
			$payment->order_number = $orderNumber;
			$payment->payment_type_code = $paymentTypeCode;
			$payment->status = $status;
			$payment->transaction_id = $transactionId;
			$paymentId = Model_Payment::addPayment($db, $payment);
		}
		
		public static function changePayStatusTo($db, $orderNumber, $status) {
			$payment = new RelObj_payment();
			$payment->order_number = $orderNumber;
			$payment->payment_type_code = $paymentTypeCode;
			$payment->status = $status;
			$payment->transaction_id = $transactionId;
			$paymentId = Model_Payment::addPayment($db, $payment);
		}
		
		public static function setPaid($db, $orderNumber) {
			$q1 = "update orders set paid = 1 where number = {$orderNumber}";
			$rs = $db->exec($q1);
		}
		
		public static function isPaid($db, $orderNumber) {
			$q1 = "select paid from orders where number = {$orderNumber}";
			$rs = $db->exec($q1);
			$isPaid = false;
			if($rs->hasNext()) {
				$row = $rs->getNextRow();
				$isPaid = $row['paid'] == 1 ? true : false;
			}
			return $isPaid;
		}
		
		public static function getCustomerEmail($db, $orderNumber) {
			$q1 = "select c.email as email from orders o, customer c where o.customer_id = c.id and o.number = {$orderNumber}";
			$rs = $db->exec($q1);
			$email = null;
			if($rs->hasNext()) {
				$row = $rs->getNextObject();
				$email = $row->email;
			}
			return $email;
		}
		
		public static function incompleteOrderExists($session) {
			$orderNumber = false;
			if(isset($session['orderNumber'])) {
				$orderNumber = $session['orderNumber'];
			}
			return $orderNumber;
		}
		
		
		public static function orderComplete($db, &$session) {
			unset($session['orderNumber']);
		}
		
		public static function getDeliveryFee($db, $orderNumber) {
			return self::getOrderAttr($db, $orderNumber, 'delivery_fee');
		}
		
		private static function addOrder($db, $order) {
			$a = new Insert($db, $order);
			$r = $a->run();
			if($r) {
				return $db->getAutoIncrementValue();
			}
			return $r;
		}
		
		public static function getOrder($db, $orderNumber) {
			$q1 = "SELECT * FROM orders WHERE number = {$orderNumber}";
			$rs = $db->exec($q1);
			$order = null;
			if($rs->hasNext()) {
				$order = $rs->getNextObject();
			}
			return $order;
		}
		
		private static function updateOrderCustomerId($db, $orderNumber, $customerId) {
			$q1 = "UPDATE orders SET customer_id = {$customerId} WHERE number = {$orderNumber}";
			$rs = $db->exec($q1);
		}
		
		private static function addOrderItems($db, $orderNumber, $cartItems) {
			foreach($cartItems as $cartItem) {
				$orderItem = new RelObj_order_item();
				$orderItem->order_number = $orderNumber;
				$orderItem->product_id = $cartItem['product_id'];
				$orderItem->quantity = $cartItem['quantity'];
				$orderItem->currency_code = $cartItem['currency_code'];
				$orderItem->price = $cartItem['price'];
				$a = new Insert($db, $orderItem);
				$a->run();
			}
		}
		
		public static function getOrderItems($db, $orderNumber, $languageCode, $currencyCode) {
			if(!$orderNumber) {
				throw new Ex("Invalid \$orderNumber");
			}
			else if(!$languageCode) {
				throw new Ex("Invalid \$languageCode");
			}
			else if(!$currencyCode) {
				throw new Ex("Invalid \$currencyCode");
			}
			
			$cartItems = array();
			$q1 = <<<EOS
				SELECT 
					i.id AS item_id,
					i.product_id AS product_id,
					GROUP_CONCAT(ia.name SEPARATOR '|') AS item_attr_names,
					GROUP_CONCAT(ia.value SEPARATOR '|') AS item_attr_values,
					ip.price AS price,
					cur.prefix AS currency_prefix,
					oi.quantity AS quantity,
					cur.code AS currency_code
				FROM order_item oi, item i, item_attr ia, item_price ip, currency cur 
				WHERE 
					i.id = ia.item_id AND 
					i.id = ip.item_id AND 
					ip.currency_code = cur.code AND
					i.product_id = oi.product_id AND
					oi.order_number = {$orderNumber} AND 
					ia.language_code = '{$languageCode}' AND 
					ip.currency_code = '{$currencyCode}' 
				GROUP BY i.id 
EOS;
			$rs = $db->exec($q1);
			if($rs->hasNext()) {
				$objects = $rs->getAllObjects();
				$cartItems = Model_Cart::objectsToCartItems($objects);
			}
			return $cartItems;
		}
		
		private static function clearOrderItems($db, $orderNumber) {
			$q1 = "DELETE FROM order_item WHERE order_number = {$orderNumber}";
			$rs = $db->exec($q1);
		}
		
		private static function addOrderAttr($db, $orderNumber, $name, $value) {
			$orderAttr = new RelObj_order_attr();
			$orderAttr->order_number = $orderNumber;
			$orderAttr->name = $name;
			$orderAttr->value = $value;
			$a = new Insert($db, $orderAttr);
			$a->run();
		}
		
		private static function getOrderAttr($db, $orderNumber, $name) {
			$q1 = "SELECT value FROM order_attr WHERE order_number = '{$orderNumber}' AND name = '{$name}'";
			$rs = $db->exec($q1);
			$value = null;
			if($rs->hasNext()) {
				$row = $rs->getNextRow();
				$value = $row['value'];
			}
			return $value;
		}
		
		private static function clearOrderAttrs($db, $orderNumber) {
			$q1 = "DELETE FROM order_attr WHERE order_number = {$orderNumber}";
			$rs = $db->exec($q1);
		}
	}

?>