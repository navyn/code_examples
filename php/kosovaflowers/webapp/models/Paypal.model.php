<?php

	require_once(dirname(__FILE__)."/../../pp/pp.php");
	require_once(WEB_FRAMEWORK_PATH."/lib/Http.lib.php");
	require_once(WEB_FRAMEWORK_PATH."/Ex.exception.php");

	class Paypal {
		const API_VERSION = '71.0';
		const SANDBOX_API_URL = 'https://api-3t.sandbox.paypal.com/nvp';
		const SANDBOX_EXPRESS_CHECKOUT_URL = 
			'https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token=';
		const API_URL = 'https://api-3t.paypal.com/nvp';
		const EXPRESS_CHECKOUT_URL = 
			'https://www.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token=';
	
		public static function setExpressCheckout($user, $pwd, $signature, $sandbox, $returnUrl, $cancelUrl, $currencyCode, $invoiceNum, $cartItems, $deliveryFee) {
			
			$apiUrl = $sandbox ? Paypal::SANDBOX_API_URL : Paypal::API_URL;
			$expressCheckoutUrl = $sandbox ? Paypal::SANDBOX_EXPRESS_CHECKOUT_URL : Paypal::EXPRESS_CHECKOUT_URL;
			
			$tran = true;
			
			$postData = array(
				'USER' => $user,
				'PWD' => $pwd,
				'SIGNATURE' => $signature,
				'VERSION' => Paypal::API_VERSION,
				'METHOD' => 'SetExpressCheckout',
				'ALLOWNOTE' => 0,
				'PAYMENTREQUEST_0_INVNUM' => $invoiceNum,
				'PAYMENTREQUEST_0_PAYMENTACTION' => 'Sale',
				'PAYMENTREQUEST_0_CURRENCYCODE' => strtoupper($currencyCode),
				'PAYMENTREQUEST_0_AMT' => '0.00',
				'RETURNURL' => $returnUrl,
				'CANCELURL' => $cancelUrl
			);
			
			if($cartItems) {
				$amt = 0.00;
				$productDesc = "";
				foreach($cartItems as $i => $cartItem) {
					$productDesc .= $cartItem['name'] . " x {$cartItem['quantity']}, ";
					$amt += $cartItem['price'] * $cartItem['quantity'];
				}
				$productDesc .= "Delivery Fee, ";
				$amt += $deliveryFee;
				$currencyCodeUpper = strtoupper($currencyCode);
				$amtFormat = number_format($amt, 2);
				$postData['PAYMENTREQUEST_0_AMT'] = $amtFormat;
				$postData['PAYMENTREQUEST_0_DESC'] = $productDesc . " ({$currencyCodeUpper} {$amtFormat})";
			}
			
			$response = Http::postForm($apiUrl, $postData);
			$responseData = Http::getPostData($response);

			if(isset($responseData['TOKEN'])) {
				$token = urldecode($responseData['TOKEN']);
				$redirectUrl = $expressCheckoutUrl . $token;
				//View::redirect($redirectUrl);
			}
			else {
				throw new Ex(
					"{$responseData['L_SEVERITYCODE0']}: {$responseData['L_SHORTMESSAGE0']}. {$responseData['L_LONGMESSAGE0']}",
					$responseData['L_ERRORCODE0']
				);
			}
			
			return new ViewRedirect($redirectUrl);
		}
		
		public static function getExpressCheckoutDetails($user, $pwd, $signature, $sandbox, $token) {
			$apiUrl = $sandbox ? Paypal::SANDBOX_API_URL : Paypal::API_URL;
			$expressCheckoutUrl = $sandbox ? Paypal::SANDBOX_EXPRESS_CHECKOUT_URL : Paypal::EXPRESS_CHECKOUT_URL;
			
			$postData = array(
				'USER' => $user,
				'PWD' => $pwd,
				'SIGNATURE' => $signature,
				'VERSION' => Paypal::API_VERSION,
				'METHOD' => 'GetExpressCheckoutDetails',
				'TOKEN' => $token
			);
			
			$response = Http::postForm($apiUrl, $postData);
			return $response;
		}
		
		public static function doExpressCheckoutPayment($user, $pwd, $signature, $sandbox, $token, $payerId, $amt, $currencyCode) {
			$apiUrl = $sandbox ? Paypal::SANDBOX_API_URL : Paypal::API_URL;
			$expressCheckoutUrl = $sandbox ? Paypal::SANDBOX_EXPRESS_CHECKOUT_URL : Paypal::EXPRESS_CHECKOUT_URL;
			
			$postData = array(
				'USER' => $user,
				'PWD' => $pwd,
				'SIGNATURE' => $signature,
				'VERSION' => Paypal::API_VERSION,
				'METHOD' => 'DoExpressCheckoutPayment',
				'TOKEN' => urlencode($token),
				'PAYERID' => $payerId,
				'PAYMENTREQUEST_0_PAYMENTACTION' => 'Sale',
				'PAYMENTREQUEST_0_CURRENCYCODE' => strtoupper($currencyCode),
				'PAYMENTREQUEST_0_AMT' => $amt
			);
			
			$response = Http::postForm($apiUrl, $postData);
			return $response;
		}
		
	}

?>