<?php

	require_once(dirname(__FILE__)."/../../pp/pp.php");
	require_once(WEB_FRAMEWORK_PATH."/mvc/OldController.class.php");
	require_once(WEB_FRAMEWORK_PATH."/mvc/View.class.php");
	require_once(MODEL_PATH."/TwoCheckout.model.php");
	require_once(MODEL_PATH."/Localization.model.php");
	require_once(MODEL_PATH."/Order.model.php");
	require_once(MODEL_PATH."/Payment.model.php");
	require_once(MODEL_PATH."/Email.model.php");
	require_once(WEB_FRAMEWORK_PATH."/Database.class.php");
	require_once(WEB_FRAMEWORK_PATH."/lib/Log.lib.php");
	
	/*
		Waits for 2checkout INS notifications.
	*/
	
	class TwoCheckoutNotifications extends OldController {
		
		public function process(&$request, &$session, $server) {
			global $dbConf;
			global $twoCheckoutConf;
			global $notificationsLog;
			
			// log notifications
			Log::writeTo($notificationsLog, print_r($request, true));
			
			if($request['message_type'] == 'FRAUD_STATUS_CHANGED') {
				$db = Database::getInstance($dbConf);
				$secretWord = $twoCheckoutConf['secretWord'];
				
				// potential confusion here:
				// vendor_order_id is the orderNumber passed as merchant_order_id
				// order_number is the unique transaction id for two checkout
				$orderNumber = $request['vendor_order_id'];
				$transactionId = $request['sale_id'];

				// if key valid then send email and show success view
				$validHash = TwoCheckout::validateNotification($request, $secretWord);
				if($validHash = true) {
					if($request['message_type'] == 'FRAUD_STATUS_CHANGED') {
						if($request['fraud_status'] == 'pass') {
							Model_Payment::updatePaymentStatus($db, $orderNumber, Model_Payment::STATUS_CAPTURED);
							Model_Order::setPaid($db, $orderNumber);
						}
						else if($request['fraud_status'] == 'fail') {
							Model_Payment::updatePaymentStatus($db, $orderNumber, Model_Payment::STATUS_FRAUDULENT);
						}
						else if($request['fraud_status'] == 'wait') {
						
						}
						else {
						
						}
					}
					/*
					$customerEmail = Model_Order::getCustomerEmail($db, $orderNumber);
					Model_Email::sendMultipart(
						$customerEmail, 
						strtr($orderEmail['subject'], array('{orderNumber}' => $orderNumber)), 
						$orderEmail['textBody'],
						$orderEmail['htmlBody'], 
						$orderEmail['from'],
						$orderEmail['bcc']
					);
					*/
				}
			}
		}
	}

?>