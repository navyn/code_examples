<?php
	
	require_once(dirname(__FILE__)."/../../pp/pp.php");
	require_once(WEB_FRAMEWORK_PATH."/mvc/OldController.class.php");
	require_once(WEB_FRAMEWORK_PATH."/mvc/View.class.php");
	require_once(WEB_FRAMEWORK_PATH."/Database.class.php");
	require_once(MODEL_PATH."/Cart.model.php");
	
	class Banner extends OldController {
		protected function process(&$request, &$session, $server) {
			global $dbConf;
			$db = Database::getInstance($dbConf);
			$cartItemCount = Model_Cart::countCartItems($db, $session);
			$model = $cartItemCount;
			$view = new View("banner.php", $model);
			return $view;
		}
	}


?>