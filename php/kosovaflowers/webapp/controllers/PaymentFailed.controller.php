<?php

	require_once(dirname(__FILE__)."/../../pp/pp.php");
	require_once(WEB_FRAMEWORK_PATH."/mvc/OldController.class.php");
	require_once(WEB_FRAMEWORK_PATH."/mvc/ViewRedirectRel.class.php");
	require_once(MODEL_PATH."/Localization.model.php");
	
	class PaymentFailed extends OldController {
		public function process(&$request, &$session, $server) {
			$errorMsg = "There was a problem processing your payment. Please try again.";
			$view = new ViewRedirectRel("delivery?error=".urlencode($errorMsg));
			return $view;
		}
	}


?>