<?php
	require_once(dirname(__FILE__)."/../../pp/pp.php");
	require_once(WEB_FRAMEWORK_PATH."/Database.class.php");
	require_once(WEB_FRAMEWORK_PATH."/UrlMapper.class.php");
	require_once(WEB_FRAMEWORK_PATH."/mvc/Controller.class.php");
	require_once(MODEL_PATH."/Localization.model.php");
	require_once(MODEL_PATH."/Item.model.php");
	
	class ItemDispatchers extends Controller {
		protected function process($httpRequest, $httpResponse) {
			global $dbConf;
			$db = Database::getInstance($dbConf);
			$seoNames = Model_Item::getSeoNames($db, $languageCode);
			
			foreach($seoNames as $seoName) {
				if(UrlMapper::makeControllerDispatcher("items/{$seoName['value']}", "Item", true)) {
					print "items/{$seoName['value']} => Item ... CREATED<br/>";
				}
			}
		}
	}
	
?>