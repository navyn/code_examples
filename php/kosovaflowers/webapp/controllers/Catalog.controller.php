<?php

	require_once(dirname(__FILE__)."/../../pp/pp.php");
	require_once(WEB_FRAMEWORK_PATH."/mvc/Controller.class.php");
	require_once(WEB_FRAMEWORK_PATH."/mvc/View.class.php");
	require_once(WEB_FRAMEWORK_PATH."/mvc/ViewCode.class.php");
	require_once(WEB_FRAMEWORK_PATH."/mvc/ViewRedirect.class.php");
	require_once(WEB_FRAMEWORK_PATH."/Database.class.php");
	require_once(WEB_FRAMEWORK_PATH."/lib/Http.lib.php");
	require_once(MODEL_PATH."/Localization.model.php");
	require_once(MODEL_PATH."/Item.model.php");
	require_once(MODEL_PATH."/Cart.model.php");
	require_once(MODEL_PATH."/Catalog.model.php");
	
	class Catalog extends Controller {
		public function process($httpRequest, $httpResponse) {
			$server = $httpRequest->server;
			$session = $httpRequest->session;
			$get = $httpRequest->get;
			
			$requestUri = trim($server['REQUEST_URI'], "/");
			$requestUriComponents = explode("/", $requestUri);
			$catalogSeoName = $requestUriComponents[count($requestUriComponents) - 1];
			
			global $dbConf;
			$db = Database::getInstance($dbConf);
			
			$languageCode = isset($get['language_code']) ? $get['language_code'] : Model_Localization::getLanguageCode($session);
			$currencyCode = isset($get['currency_code']) ? $get['currency_code'] : Model_Localization::getCurrencyCode($session);
			Model_Localization::setLanguageCode($session, $languageCode);
			Model_Localization::setCurrencyCode($session, $currencyCode);
			
			$catalogName = Model_Catalog::getCatalogName($db, $catalogSeoName);
			$items = Model_Catalog::getCatalogItems($db, $catalogSeoName, $languageCode, $currencyCode);
			$model = array($catalogName, $items);
			$view = new View("catalog.php", $model);
			return $view;
			/*
			$response = Http::postForm(ROOT_URL."/", array('catalog_name' => "flowers", 'language_code' => 'en'), session_id());
			return new ViewCode($response);
			*/
			return new ViewRedirect(ROOT_URL."?catalog_name=flowers");
		}
		
	}
?>