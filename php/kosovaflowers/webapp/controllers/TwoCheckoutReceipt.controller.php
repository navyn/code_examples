<?php

	require_once(dirname(__FILE__)."/../../pp/pp.php");
	require_once(WEB_FRAMEWORK_PATH."/mvc/OldController.class.php");
	require_once(WEB_FRAMEWORK_PATH."/mvc/View.class.php");
	require_once(WEB_FRAMEWORK_PATH."/mvc/ViewRedirectRel.class.php");
	require_once(MODEL_PATH."/TwoCheckout.model.php");
	require_once(MODEL_PATH."/Localization.model.php");
	require_once(MODEL_PATH."/Order.model.php");
	require_once(MODEL_PATH."/Email.model.php");
	require_once(WEB_FRAMEWORK_PATH."/Database.class.php");
	
	/*
		Waits for 2checkout to return.
	*/
	
	class TwoCheckoutReceipt extends OldController {
		
		public function process(&$request, &$session, $server) {
			global $dbConf;
			global $twoCheckoutConf;
			
			if($request['credit_card_processed'] == 'Y') {
				$db = Database::getInstance($dbConf);
				$secretWord = $twoCheckoutConf['secretWord'];
				
				// potential confusion here:
				// cart_order_id is the orderNumber passed
				// order_number is the unique transaction id for two checkout
				$orderNumber = $request['cart_order_id'];
				$transactionId = $request['order_number'];
				
				$view = null;
				// if key valid then send email and show success view
				$validKey = TwoCheckout::validateReceipt($request, $secretWord);
				if($validKey = true) {
					Model_Order::pay($db, $orderNumber, Model_Payment::TWOCHECKOUT, Model_Payment::STATUS_AUTHORIZED, $transactionId);
					
					$view = new ViewRedirectRel("payment-success?orderNumber={$orderNumber}");
				}
				else {
					$view = new View('paymentfailed.php');
				}

				return $view;
			}
		}
	}

?>