<?php

	require_once(dirname(__FILE__)."/../../pp/pp.php");
	require_once(WEB_FRAMEWORK_PATH."/Database.class.php");
	require_once(WEB_FRAMEWORK_PATH."/mvc/OldController.class.php");
	require_once(WEB_FRAMEWORK_PATH."/mvc/View.class.php");
	require_once(WEB_FRAMEWORK_PATH."/mvc/ViewRedirectRel.class.php");
	require_once(MODEL_PATH."/Localization.model.php");
	require_once(MODEL_PATH."/Cart.model.php");
	require_once(MODEL_PATH."/Order.model.php");
	require_once(MODEL_PATH."/Payment.model.php");
	require_once(MODEL_PATH."/Delivery.model.php");
	require_once(MODEL_PATH."/Email.model.php");
	
	class PaymentSuccess extends OldController {
		public function process(&$request, &$session, $server) {
			$view = new ViewRedirectRel("");
			if(isset($request['orderNumber']) &&
				Model_Order::incompleteOrderExists($session) == $request['orderNumber']) {
				
				global $dbConf;
				$db = Database::getInstance($dbConf);
				$invoiceNum = $request['orderNumber'];
				
				$paymentType = Model_Payment::getPaymentType($db, $invoiceNum);
				$paymentStatus = Model_Payment::getPaymentStatus($db, $invoiceNum);
				
				$successful = false; 
				if($paymentType == Model_Payment::PAYPAL_EXPRESS) {
					$successful = $paymentStatus == Model_Payment::STATUS_CAPTURED ? true : false;
				}
				else if($paymentType == Model_Payment::TWOCHECKOUT) {
					$successful = $paymentStatus == Model_Payment::STATUS_AUTHORIZED || $paymentStatus == Model_Payment::STATUS_CAPTURED ? true : false;
				}
				
				if($successful) {
					$languageCode = Model_Localization::getLanguageCode($session);
					$currencyCode = Model_Localization::getCurrencyCode($session);
					$cartItems = Model_Order::getOrderItems($db, $invoiceNum, $languageCode, $currencyCode);
					$deliveryFee = Model_Order::getDeliveryFee($db, $invoiceNum);
					$customerEmail = Model_Order::getCustomerEmail($db, $invoiceNum);
					$delivery = Model_Delivery::getDelivery($db, $invoiceNum);
					
					// make receipt html version
					$orderEmailModel = array($delivery, $cartItems, $deliveryFee);
					$orderEmailHtml = new View("orderemail.html.php", $orderEmailModel);
					$orderEmailHtml = $orderEmailHtml->getOutput();
					$orderEmailText = new View("orderemail.text.php", $orderEmailModel);
					$orderEmailText = $orderEmailText->getOutput();
					
					// send email html and text version
					global $orderEmail;
					Model_Email::sendMultipart(
						$customerEmail, 
						$orderEmail['subject'], 
						$orderEmailText,
						$orderEmailHtml, 
						$orderEmail['from'],
						$orderEmail['replyTo'],
						$orderEmail['bcc']
					);
					
					Model_Cart::cartConversion($db, $session);
					Model_Order::orderComplete($db, $session);
					$model = array($customerEmail, $cartItems, $delivery, $deliveryFee);
					$view  = new View('paymentsuccess.php', $model);
				}
			}
			return $view;
		}
	}


?>