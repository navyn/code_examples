<?php

	require_once(dirname(__FILE__)."/../../pp/pp.php");
	require_once(WEB_FRAMEWORK_PATH."/mvc/Controller.class.php");
	require_once(WEB_FRAMEWORK_PATH."/mvc/View.class.php");
	require_once(WEB_FRAMEWORK_PATH."/mvc/ViewCode.class.php");
	require_once(WEB_FRAMEWORK_PATH."/mvc/ViewRedirect.class.php");
	require_once(WEB_FRAMEWORK_PATH."/Database.class.php");
	require_once(WEB_FRAMEWORK_PATH."/lib/Http.lib.php");
	require_once(MODEL_PATH."/Localization.model.php");
	require_once(MODEL_PATH."/Item.model.php");
	require_once(MODEL_PATH."/Cart.model.php");
	require_once(MODEL_PATH."/Catalog.model.php");
	
	class StoreFront extends Controller {
		public function process($httpRequest, $httpResponse) {
			$server = $httpRequest->server;
			$session = $httpRequest->session;
			$request = array_merge($httpRequest->get, $httpRequest->post);
			
			if(isset($request['catalog_name'])) {
				global $dbConf;
				$db = Database::getInstance($dbConf);
				$catalogName = $request['catalog_name'];
				$languageCode = isset($request['language_code']) ? $request['language_code'] : Model_Localization::getLanguageCode($session);
				$currencyCode = isset($request['currency_code']) ? $request['currency_code'] : Model_Localization::getCurrencyCode($session);
				Model_Localization::setLanguageCode($session, $languageCode);
				Model_Localization::setCurrencyCode($session, $currencyCode);
				$items = Model_Catalog::getCatalogItems($db, $catalogName, $languageCode, $currencyCode);
				$model = $items;
				$view = new View("storefront.php", $model);
				return $view;
			}
			
			/*
			$catalogResponse = Http::postForm(ROOT_URL."/", array('catalog_name' => "chocolates", 'language_code' => 'en'), null, session_id());
			$catalogHeaderContent = Http::getHeaderContent($catalogResponse);
			return new ViewCode($catalogHeaderContent[1]);
			*/
				
			return new ViewRedirect(ROOT_URL."?catalog_name=flowers");
		}
		
	}
?>