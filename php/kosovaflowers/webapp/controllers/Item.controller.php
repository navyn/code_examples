<?php
	
	require_once(dirname(__FILE__)."/../../pp/pp.php");
	require_once(WEB_FRAMEWORK_PATH."/mvc/OldController.class.php");
	require_once(WEB_FRAMEWORK_PATH."/mvc/View.class.php");
	require_once(WEB_FRAMEWORK_PATH."/mvc/ViewCode.class.php");
	require_once(WEB_FRAMEWORK_PATH."/mvc/ViewRedirect.class.php");
	require_once(WEB_FRAMEWORK_PATH."/Database.class.php");
	require_once(WEB_FRAMEWORK_PATH."/lib/Http.lib.php");
	require_once(MODEL_PATH."/Localization.model.php");
	require_once(MODEL_PATH."/Item.model.php");

	class Item extends OldController {
		public function process(&$request, &$session, $server) {
			global $dbConf;
			$db = Database::getInstance($dbConf);
			$languageCode = Model_Localization::getLanguageCode($session);
			$currencyCode = Model_Localization::getCurrencyCode($session);
			
			$requestUri = trim($server['REQUEST_URI'], "/");
			$requestUriComponents = explode("/", $requestUri);
			$seoName = $requestUriComponents[count($requestUriComponents) - 1];
			$itemId = $this->getItemIdBySeoName($db, $seoName);
			$item = $this->getItemById($db, $itemId, $languageCode, $currencyCode);
			
			$view = new View("item.php", $item);
			return $view;
		}
		
		private function getItemIdBySeoName($db, $seoName) {
			$seoName = $db->escapeString($seoName);
			$rs = $db->exec("select ia.item_id from item_attr ia where ia.name = 'seo_name' and ia.value = '{$seoName}' and ia.status = 'ACTIVE'");
			$itemId = null;
			if($rs->hasNext()) {
				$object = $rs->getNextObject();
				$itemId = $object->item_id;
			}
			return $itemId;
		}
		
		private function getItemById($db, $itemId, $languageCode, $currencyCode) {
			$rs = $db->exec("select ce.id as catalog_entry_id, i.id as item_id, i.product_id as product_id, c.name as catalog_name, ce.sort_by as catalog_entry_sort_by, group_concat(id.name separator '|') as item_attr_names, group_concat(id.value separator '|') as item_attr_values, ip.price as price, cur.prefix as currency_prefix from catalog c, catalog_entry ce, item i, item_attr id, item_price ip, currency cur where c.id = ce.catalog_id and ce.item_id = i.id and i.id = id.item_id and i.id = ip.item_id and ip.currency_code = cur.code and i.id = {$itemId} and id.language_code = '{$languageCode}' and ip.currency_code = '{$currencyCode}' group by ce.id, i.id");
			$objects = array();
			if($rs->hasNext()) {
				$objects = $rs->getAllObjects();
			}
			$items = Model_Item::objectsToItems($objects);
			return $items[0];
		}
	}
	
?>