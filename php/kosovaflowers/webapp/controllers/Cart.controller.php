<?php
	require_once(__DIR__."/../../pp/pp.php");
	require_once(WEB_FRAMEWORK_PATH."/mvc/OldController.class.php");
	require_once(WEB_FRAMEWORK_PATH."/mvc/View.class.php");
	require_once(WEB_FRAMEWORK_PATH."/mvc/ViewRedirectRel.class.php");
	require_once(WEB_FRAMEWORK_PATH."/Database.class.php");
	require_once(WEB_FRAMEWORK_PATH."/lib/Http.lib.php");
	require_once(MODEL_PATH."/Localization.model.php");
	require_once(MODEL_PATH."/Cart.model.php");
	require_once(MODEL_PATH."/GoogleCheckout.model.php");
	require_once(MODEL_PATH."/Paypal.model.php");
	require_once(MODEL_PATH."/PaypalStandard.model.php");
	require_once(MODEL_PATH."/TwoCheckout.model.php");
	
	class Cart extends OldController {
		public function process(&$request, &$session, $server) {
			global $dbConf;
			global $paypalAuth;
			
			$db = Database::getInstance($dbConf);
			$languageCode = Model_Localization::getLanguageCode($session);
			$currencyCode = Model_Localization::getCurrencyCode($session);
			$currencyPrefix = Model_Localization::getCurrencyPrefix($db, $session);
			$cartId = Model_Cart::getCartId($session);
			
			if(isset($request['do'])) {
				if($request['do'] == 'add') {
					$itemId = $request['itemId'];
					$quantity = 1;
					Model_Cart::addToCart($db, $session, $itemId, $quantity);
				}
				else if($request['do'] == 'remove') {
					$itemId = $request['itemId'];
					$quantity = 1;
					Model_Cart::removeFromCart($db, $session, $itemId, $quantity);
				}
			}
			
			if(isset($request['checkout'])) {
			
				if($request['checkout'] == 'Checkout') {
					return new ViewRedirectRel("delivery");
				}
				
				/*
				if($request['checkout'] == 'paypalstandard') {
					$cartItems = Model_Cart::getCartItems($db, $session, $languageCode, $currencyCode);
					$paypalReturnUrl = $paypalAuth['return_url'];
					$paypalCancelUrl = $paypalAuth['cancel_url'];
					$view = PaypalStandard::upload(
						'shop1_1293463125_biz@telnet7.net',
						$cartItems,
						$currencyCode,
						$paypalReturnUrl,
						$paypalCancelUrl
					);
					return $view;
				}
				*/
			}
			
			$cartItems = Model_Cart::getCartItems($db, $session, $languageCode, $currencyCode);
			
			$msg = null;
			if(isset($request['msg'])) {
				$msg = urldecode($request['msg']);
			}
			
			$model = array($cartItems, $currencyPrefix, $msg);
			return new View("cart.php", $model);
		}
		
	}
	
?>